

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0002_auto_20200813_1721'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='course',
            name='end_select',
        ),
        migrations.RemoveField(
            model_name='course',
            name='is_end',
        ),
        migrations.RemoveField(
            model_name='course',
            name='start_select',
        ),
        migrations.AddField(
            model_name='course',
            name='status',
            field=models.IntegerField(default=1, verbose_name='课程状态'),
        ),
    ]
