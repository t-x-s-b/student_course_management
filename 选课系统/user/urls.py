from django.urls import path
from user import views


urlpatterns = [
    path('login/', views.home, name="login"),
    path('login/<slug:kind>', views.login, name="login"),
    path('register/<slug:kind>', views.register, name="register"),

    path('update/<slug:kind>', views.update, name="update"),
    path('logout/', views.logout, name="logout"),
    path('student_delete/<int:student_id>', views.student_delete, name="student_delete"),
    path('teacher_delete/<int:teacher_id>', views.teacher_delete, name="teacher_delete"),
    path('student_update/<int:student_id>', views.student_update, name="student_update"),
    path('teacher_update/<int:teacher_id>', views.teacher_update, name="teacher_update"),
]
