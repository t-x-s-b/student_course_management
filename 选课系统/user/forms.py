# usr/bin/env python
# -*- coding:utf-8- -*-
from django import forms
from .models import Student, Teacher


class StuLoginForm(forms.Form):
    uid = forms.CharField(label='学号', max_length=10)
    password = forms.CharField(label='密码', max_length=30, widget=forms.PasswordInput)


class TeaLoginForm(forms.Form):
    uid = forms.CharField(label='教职工号', max_length=10)
    password = forms.CharField(label='密码', max_length=30, widget=forms.PasswordInput)


class StuRegisterForm(forms.ModelForm):
    # 提示信息
    birthday = forms.DateField(label="生日", widget=forms.TextInput(attrs={'placeholder': '2055-8-8'}))
    email = forms.EmailField(label="邮箱", widget=forms.TextInput(attrs={'placeholder': '123@qq.com'}))

    confirm_password = forms.CharField(widget=forms.PasswordInput(), label="确认密码")

    class Meta:
        model = Student
        fields = ('grade',
                  'name',
                  'password',
                  'confirm_password',
                  'gender',
                  'birthday',
                  'email',
                  'info')

    def clean(self):
        cleaned_data = super(StuRegisterForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')
        if confirm_password != password:
            self.add_error('confirm_password', 'Password does not match.')

        grade = cleaned_data.get('grade')
        if not grade.isdigit():
            self.add_error('grade', '年级:请输入4位数字')
        return cleaned_data


class StuUpdateForm(StuRegisterForm):
    class Meta:
        model = Student
        fields = ('name',
                  'password',
                  'confirm_password',
                  'gender',
                  'birthday',
                  'email',
                  'info',
                  )


class TeaRegisterForm(forms.ModelForm):
    birthday = forms.DateField(label="生日", widget=forms.TextInput(attrs={'placeholder': '2035-6-16'}))
    email = forms.EmailField(label="邮箱", widget=forms.TextInput(attrs={'placeholder': '123@qq.com'}))

    confirm_password = forms.CharField(widget=forms.PasswordInput(), label="确认密码")

    class Meta:
        model = Teacher
        fields = ('name',
                  'password',
                  'confirm_password',
                  'gender',
                  'birthday',
                  'email',
                  'info',)

    def clean(self):
        cleaned_data = super(TeaRegisterForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')
        if confirm_password != password:
            self.add_error('confirm_password', 'Password does not match.')

#############################
from .models import Admin

class AdminLoginForm(forms.Form):
    uid = forms.CharField(label='管理员号', max_length=10)
    password = forms.CharField(label='密码', max_length=30, widget=forms.PasswordInput)

class AdminRegisterForm(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput(), label="确认密码")

    class Meta:
        model = Admin
        fields = ('admin_pr', 'password')

    def clean(self):
        cleaned_data = super(AdminRegisterForm, self).clean()
        password = cleaned_data.get('password')
        confirm_password = cleaned_data.get('confirm_password')
        if confirm_password != password:
            self.add_error('confirm_password', 'Password does not match.')

        pr = cleaned_data.get('admin_pr')
        if pr != 'root':
            self.add_error('admin_pr', '非法权限')

        return cleaned_data