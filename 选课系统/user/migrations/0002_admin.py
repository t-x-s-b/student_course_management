
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('admin_pr', models.CharField(max_length=4, verbose_name='权限')),
                ('admin_no', models.CharField(max_length=10, verbose_name='管理员号')),
                ('number', models.CharField(max_length=6, verbose_name='权限编号')),
                ('email', models.EmailField(max_length=254, verbose_name='邮箱')),
                ('password', models.CharField(max_length=30, verbose_name='密码')),
            ],
        ),
    ]
