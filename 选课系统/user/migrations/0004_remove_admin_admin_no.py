
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_remove_admin_email'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='admin',
            name='admin_no',
        ),
    ]
