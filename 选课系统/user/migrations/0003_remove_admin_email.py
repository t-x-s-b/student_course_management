
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_admin'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='admin',
            name='email',
        ),
    ]
